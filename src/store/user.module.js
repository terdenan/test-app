export const user = {
    namespaced: true,
    state: {
        email: "",
        first_name: "",
        second_name: "",
        phone: "",
        status: undefined,
        group: undefined,
    },
    mutations: {
        setInfo(state, {email, first_name, second_name, phone}) {
            state.email = email;
            state.first_name = first_name;
            state.second_name = second_name;
            state.phone = phone;
        },
        setGroup(state, group) {
            state.group = group;
        },
        setStatus(state, status) {
            state.status = status;
        }
    },
};